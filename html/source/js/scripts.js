$(document).ready(function() {

	$('.slick').slick({
  		fade: true,
  		dots: false,
  		autoplay: true,
  		arrows: false
	 });

	$('.gallery_slick').slick({
  		fade: true,
  		autoplay: false,
	 });

	$('.more-gallery_slider').slick({
  		fade: true,
  		autoplay: false,
  		arrows: true
	 });

	$('a#fancybox').fancybox({
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false,
		'padding'		: 	0
	});
	 
	$('.offcanvas-toggle').on('click', function() {
	  $('body').toggleClass('offcanvas-expanded');
	});

	$('.nav--mobile a').on('click', function() {
		$('body').toggleClass('offcanvas-expanded');
	});
	
	$(".fancybox").fancybox();

	document.querySelector( "#nav-toggle" )
	  .addEventListener( "click", function(){
	  this.classList.toggle( "active" );
	});

	$('#pop').on('shown.bs.modal', function () {
	  $('#pop').focus()
	})

	$(function() {
	  $('a[href*="#"]:not([href="#"])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html, body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

	$('.map').click(function () {
    	$('.map iframe').css("pointer-events", "auto");
	});
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-38757348-4', 'auto');
	  ga('send', 'pageview');

	$(window).scroll(function () { 
	    console.log($(window).scrollTop());
	    if ($(window).scrollTop() > 150) {
	      $('.navigation').addClass('navbar-fixed-top');
	    }

	    if ($(window).scrollTop() < 150) {
	      $('.navigation').removeClass('navbar-fixed-top');
	    }
  	});
});


